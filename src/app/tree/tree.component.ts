import { Component, OnInit, Input } from '@angular/core';
import { treeNode } from './treeNode';

@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.css']
})
export class TreeComponent implements OnInit {

  @Input() node: treeNode;

  constructor() { 
  }

  ngOnInit() {
  }

  toggleShowChildren() {
    this.node.isExpanded = this.node.isExpanded ? false : true;
  }
}
