export class treeNode {
    name: string = 'Unnamed';
    children: treeNode[] = [];
    isExpanded?: boolean = false;
}