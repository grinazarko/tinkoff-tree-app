import { Injectable } from '@angular/core';
import { treeNode } from './treeNode';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class TreeService {

  constructor(private http: HttpClient) { }
  
  getRoot(): Observable<treeNode> { 
    return this.http.get<treeNode>('./assets/tree.json');
  }
}
