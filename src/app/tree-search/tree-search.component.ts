import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-tree-search',
  templateUrl: './tree-search.component.html',
  styleUrls: ['./tree-search.component.css']
})
export class TreeSearchComponent implements OnInit {

  @Output() inputSearch = new EventEmitter<string[]>();;

  constructor() { }

  ngOnInit() {
  }

  search(event) {
    this.inputSearch.emit(event.target.value);
  }
}
