import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { TreeComponent } from './tree/tree.component';
import { TreeService } from './tree/tree.service';
import { HttpClientModule } from '@angular/common/http';
import { TreeSearchComponent } from './tree-search/tree-search.component';


@NgModule({
  declarations: [
    AppComponent,
    TreeComponent,
    TreeSearchComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [TreeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
