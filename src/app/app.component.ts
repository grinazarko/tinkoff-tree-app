import { Component } from '@angular/core';
import { TreeService } from './tree/tree.service';
import { treeNode } from './tree/treeNode';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  treeNode: treeNode;

  constructor(private treeService: TreeService) {
    treeService.getRoot().subscribe(data => {
      this.treeNode = data;
    });
    
  }

  search(text: string) {
    this.searchInTree(text, this.treeNode);
  }

  searchInTree(text:string, node: treeNode) {
    if (text && node.name === text) {
      node.isExpanded = true;
    } else {
      node.isExpanded = false;
    }
    for (let childIndex in node.children) {
      if (this.searchInTree(text, node.children[childIndex])){
        node.isExpanded = true;
      } 
    }
    return node.isExpanded
  }
}
